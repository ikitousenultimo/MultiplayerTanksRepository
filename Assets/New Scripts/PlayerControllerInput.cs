using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(CharacterController))]
public class PlayerControllerInput : MonoBehaviour
{
    [SerializeField]
    private CharacterController controller;
    private Vector3 playerVelocity;
    private bool groundedPlayer;
    [SerializeField]
    private float playerSpeed = 2.0f;
    [SerializeField]
    private float gravityValue = -9.81f;

    public int PlayerNumber = GameStartOnClick.TanksP;
    public float m_Speed = 12f;
    public float m_TurnSpeed = 180f;
    public AudioSource MovementAudio;
    public AudioClip EngineIdling;
    public AudioClip EngineDriving;
    public float PitchRange = 0.2f;

    private Rigidbody Rigidbody;

    private Vector2 movementInput=Vector2.zero;

    private bool fire = false;
    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody>();
    }
    private void Start()
    {
        controller = gameObject.GetComponent<CharacterController>();
    }
    public void OnMove(InputAction.CallbackContext context)
    {
        movementInput = context.ReadValue<Vector2>();
    }
    public void OnFire(InputAction.CallbackContext context)
    {
        fire = context.action.triggered;
    }
    void FixedUpdate()
    {
        Vector3 move = new Vector3(0, 0,movementInput.x);
        controller.Move(move * Time.deltaTime * playerSpeed);
        Rigidbody.MovePosition(Rigidbody.position + move);
        Turn();

        if (move != Vector3.zero)
        {
            gameObject.transform.forward = move;
        }

        // Changes the height position of the player..

        controller.Move(playerVelocity * Time.deltaTime);
    }
    private void OnEnable()
    {
        Rigidbody.isKinematic = false;
    }
    private void OnDisable()
    {
        Rigidbody.isKinematic = true;
    }
    public void Turn()
    {
        // Determine the number of degrees to be turned based on the input, speed and time between frames.
        float turn = movementInput.x * m_TurnSpeed * Time.deltaTime;

        // Make this into a rotation in the y axis.
        Quaternion turnRotation = Quaternion.Euler(0f, turn, 0f);

        // Apply this rotation to the rigidbody's rotation.
        Rigidbody.MoveRotation(Rigidbody.rotation * turnRotation);
    }
}
