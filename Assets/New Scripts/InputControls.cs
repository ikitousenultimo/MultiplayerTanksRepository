using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.InputSystem;

public class InputControls : MonoBehaviour
{
    private Camera mainCam;
    private Vector3 worldPos;
    public PlayerInput playerInput;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame

    void Update()
    {
        var mousePosition = playerInput.actions["point"].ReadValue<Vector2>();
        var projectedMousePosition = mainCam.ScreenToWorldPoint(mousePosition);

    }

}