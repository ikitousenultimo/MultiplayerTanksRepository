using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplitScriptSwitch : MonoBehaviour
{
    public Camera[] Cams;
    public Camera MainCamera;
    public int NumberofCameras;
    // Start is called before the first frame update
    void Start()
    {
        Cams = FindObjectsOfType<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void OnClick()
    {
        NumberofCameras= FindObjectsOfType<Camera>().Length;
        if(NumberofCameras>=0 && NumberofCameras<=5)
        {
            if(NumberofCameras==3)
            {
                MainCamera.gameObject.SetActive(false);
                Cams[1].rect = new Rect(0f, 0f, 0.5f, 1f);
                Cams[2].rect = new Rect(-0.5f, 0f, -0.5f, 1f);
            }
            if(NumberofCameras==4)
            {
                MainCamera.gameObject.SetActive(false);
                Cams[1].rect = new Rect(0f, 0f, 1f, 0.5f);
                Cams[2].rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                Cams[3].rect = new Rect(0.5f, 0, 0.5f, 0.5f);

            }
            if(NumberofCameras==5)
            {
                MainCamera.gameObject.SetActive(false);
                Cams[1].rect = new Rect(0, 0.5f, 0.5f, 0.5f);
                Cams[2].rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                Cams[3].rect = new Rect(0, 0, 0.5f, 0.5f);
                Cams[4].rect = new Rect(0.5f, 0, 0.5f, 0.5f);
            }
            else
            {
                MainCamera.gameObject.SetActive(true);
            }
        }
    }
}
