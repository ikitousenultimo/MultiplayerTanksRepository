using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStartOnClick : MonoBehaviour
{
    public static int TanksP;
    public GameObject Boton;

    public static bool PLayer2 { get; set; }
    public static bool PLayer3 { get; set; }
    public static bool PLayer4 { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void OnClickStart()
    {
        TanksP = int.Parse(Boton.name);

    }
    public void MoveONScene()
    {
        SceneManager.LoadScene("_Complete-Game");
    }
}
